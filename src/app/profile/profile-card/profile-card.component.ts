import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {AngularFireStorage} from '@angular/fire/storage';
import {AuthenticationService} from '../../services/authentication.service';
import {User} from '../../interfaces/user';
import {UserService} from '../../services/user.service';
import {FormGroup, FormBuilder, Validators, FormControl} from '@angular/forms';
import {LyTheme2, Platform, ThemeVariables} from '@alyle/ui';
import {ImgCropperConfig, ImgCropperEvent, LyResizingCroppingImages} from '@alyle/ui/resizing-cropping-images';
import {Globals} from '../../globals';

const STYLES = (theme: ThemeVariables) => ({
  button: {
    margin: '1em'
  },
  labelBefore: {
    paddingAfter: '8px'
  },
  labelAfter: {
    paddingBefore: '8px'
  },
  iconSmall: {
    fontSize: '20px'
  },
  item: {
    // padding: '16px',
    textAlign: 'center',
    // background: theme.background.secondary,
    // boxShadow: shadowBuilder(1),
    borderRadius: '4px',
    height: '100%',
    marginTop: '16px'
  },
  // image
  actions: {
    display: 'flex'
  },
  cropping: {
    maxWidth: '400px',
    height: '300px',
    margin: '0 auto'
  },
  flex: {
    flex: 1
  },
  range: {
    textAlign: 'center',
    maxWidth: '400px',
    margin: '0 auto'
  },
  rangeInput: {
    maxWidth: '150px',
    margin: '1em 0',

    // http://brennaobrien.com/blog/2014/05/style-input-type-range-in-every-browser.html
    // removes default webkit styles
    '-webkit-appearance': 'none',

    // fix for FF unable to apply focus style bug
    border: `solid 6px ${theme.background.tertiary}`,

    // required for proper track sizing in FF
    width: '100%',
    '&::-webkit-slider-runnable-track': {
      width: '300px',
      height: '3px',
      background: '#ddd',
      border: 'none',
      borderRadius: '3px'
    },
    '&::-webkit-slider-thumb': {
      '-webkit-appearance': 'none',
      border: 'none',
      height: '16px',
      width: '16px',
      borderRadius: '50%',
      background: theme.primary.default,
      marginTop: '-6px'
    },
    '&:focus': {
      outline: 'none'
    },
    '&:focus::-webkit-slider-runnable-track': {
      background: '#ccc'
    },

    '&::-moz-range-track': {
      width: '300px',
      height: '3px',
      background: '#ddd',
      border: 'none',
      borderRadius: '3px'
    },
    '&::-moz-range-thumb': {
      border: 'none',
      height: '16px',
      width: '16px',
      borderRadius: '50%',
      background: theme.primary.default
    },

    // hide the outline behind the border
    '&:-moz-focusring': {
      outline: '1px solid white',
      outlineOffset: '-1px',
    },

    '&::-ms-track': {
      width: '300px',
      height: '3px',

      // remove bg colour from the track, we'll use ms-fill-lower and ms-fill-upper instead
      background: 'transparent',

      // leave room for the larger thumb to overflow with a transparent border
      borderColor: 'transparent',
      borderWidth: '6px 0',

      // remove default tick marks
      color: 'transparent'
    },
    '&::-ms-fill-lower': {
      background: '#777',
      borderRadius: '10px'
    },
    '&::-ms-fill-': {
      background: '#ddd',
      borderRadius: '10px',
    },
    '&::-ms-thumb': {
      border: 'none',
      height: '16px',
      width: '16px',
      borderRadius: '50%',
      background: theme.primary.default,
    },
    '&:focus::-ms-fill-lower': {
      background: '#888'
    },
    '&:focus::-ms-fill-upper': {
      background: '#ccc'
    }
  }
});

@Component({
  selector: 'app-profile-card',
  templateUrl: './profile-card.component.html',
  styleUrls: ['./profile-card.component.scss']
})
export class ProfileCardComponent implements AfterViewInit, OnInit {
  classes = this.theme.addStyleSheet(STYLES);
  croppedImage?: string;
  scale: number;
  @ViewChild(LyResizingCroppingImages) cropper: LyResizingCroppingImages;
  myConfig: ImgCropperConfig = {
    autoCrop: true,
    width: 150, // Default `250`
    height: 150, // Default `200`
    // fill: , // Default transparent if type = png else #000,
    type: 'image/jpeg'
  };
  picture: any;

  step: number = null;
  appearance = 'outlined';
  user: User;
  showContentLoadImage: boolean = false;

  userFormProfile: FormGroup;
  formErrors = {
    'nick': '',
    'country': '',
    'birthday': ''
  };
  validationMessages = {
    'nick': {
      'required': 'Please enter your nick',
    },
    'country': {
      'required': 'Please enter your country',
    },
    'birthday': {
      'required': 'Please enter your birthday date',
    }
  };

  constructor(public global: Globals, private theme: LyTheme2, private fb: FormBuilder, private userService: UserService,
              private authenticationService: AuthenticationService, private firebaseStore: AngularFireStorage) {
    this.authenticationService.getStatusSession().subscribe(status => {
      this.userService.getUserById(status.uid).valueChanges().subscribe((data: User) => {
        this.user = data;
        this.buildForm();
      }, error1 => console.log(error1));
    });
  }

  ngOnInit(): void {
    this.buildForm();
  }

  ngAfterViewInit() {
  }

  onCropped(e: ImgCropperEvent) {
    this.croppedImage = e.dataURL;
  }

  onloaded(e: ImgCropperEvent) {
    this.showContentLoadImage = true;
    this.cropper.center();
  }

  onerror(e: ImgCropperEvent) {
    console.warn(`'${e.name}' is not a valid image`, e);
  }

  buildForm() {
    this.userFormProfile = this.fb.group({
      'nick': [this.user ? this.user.nick : '',
        [
          Validators.required,
        ]
      ],
      'country': [this.user && this.user.country ? this.user.country : ''],
      'birthday': [this.user && this.user.birthday ? this.user.birthday : ''],
    });

    this.userFormProfile.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged();
  }

  onValueChanged(data?: any) {
    if (!this.userFormProfile) {
      return;
    }
    const form = this.userFormProfile;
    for (const field in this.formErrors) {
      if (this.user) {
        if (typeof this.userFormProfile.controls[field].value === 'object') {
          // @ts-ignore
          this.userFormProfile.controls[field].value = JSON.stringify(this.userFormProfile.controls[field].value);
        }
        this.user[field] = this.userFormProfile.controls[field].value;
      }
      if (Object.prototype.hasOwnProperty.call(this.formErrors, field)) {
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (Object.prototype.hasOwnProperty.call(control.errors, key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  updateImageUser() {
    if (this.croppedImage) {
      const currentPictureId = Date.now();
      const picture = this.firebaseStore.ref('picture/' + currentPictureId + '.jpg')
        .putString(this.croppedImage, 'data_url');

      picture.then(response => {
        this.picture = this.firebaseStore.ref('picture/' + currentPictureId + '.jpg').getDownloadURL();
        this.picture.subscribe(p => {
          this.userService.updateImageUser(p, this.user.uid).then(() => this.showContentLoadImage = false)
            .catch(error => console.log(error));
        });
      }).catch(error => console.log(error));
    }
  }

  updateUserData() {
    this.userService.updateUser(this.user).then(() => {
      alert('User updated');
    }).catch(error => console.log(error));
  }
}

class ProfileCardComponentImpl extends ProfileCardComponent {
}
