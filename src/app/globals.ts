import {Injectable} from '@angular/core';

Injectable()
export class Globals {
  public LOGO_URL = './assets/img/Mega-Chat-Logo-Circulo.png';
  // public BACKGROUND_IMAGE = './assets/img/background.jpg';
  // public BACKGROUND_IMAGE = './assets/img/fondo.png';
  public BACKGROUND_IMAGE = '';
  public USER_DEFAULT_URL = './assets/img/noavatar.png';
  public URL_REGISTER = '/register';
  public URL_LOGIN = '/';
  public URL_PROFILE = '/auth/profile';
}
