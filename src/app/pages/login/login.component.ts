import {Component, OnInit, Provider, SecurityContext} from '@angular/core';
import {Router} from '@angular/router';
import {LyTheme2, shadowBuilder, ThemeVariables} from '@alyle/ui';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {AuthenticationService} from '../../services/authentication.service';
import {Globals} from '../../globals';
import {DomSanitizer} from '@angular/platform-browser';

const STYLES = (theme: ThemeVariables) => ({
  button: {
    margin: '1em'
  },
  labelBefore: {
    paddingAfter: '8px'
  },
  item: {
    padding: '16px',
    textAlign: 'center',
    background: theme.background.secondary,
    boxShadow: shadowBuilder(1),
    borderRadius: '4px',
    height: '100%'
  },
  link: {
    color: theme.text.secondary
  }
});


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  readonly classes = this._theme.addStyleSheet(STYLES);
  hide = true;
  appearance = 'outlined';
  count_password: number;

  userForm: FormGroup;
  formErrors = {
    'email': '',
    'password': ''
  };
  validationMessages = {
    'email': {
      'required': 'Please enter your email',
      'email': 'Please enter your vaild email'
    },
    'password': {
      'required': 'Please enter your password',
      'pattern': 'The password must contain numbers and letters',
      'minlength': 'Please enter more than 8 characters',
      'maxlength': 'Please enter less than 25 characters',
    }
  };

  constructor(private router: Router, private _theme: LyTheme2, private fb: FormBuilder,
              private authenticationService: AuthenticationService, public global: Globals,
              private sanitizer: DomSanitizer) {
  }

  getBackground(img) {
    return this.sanitizer.sanitize(SecurityContext.STYLE, 'url(' + img + ')');
  }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.userForm = this.fb.group({
      'email': ['', [
        Validators.required,
        Validators.email
      ]
      ],
      'password': ['', [
        Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$'),
        Validators.minLength(8),
        Validators.maxLength(25)
      ]
      ],
    });

    this.userForm.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged();
  }

  onValueChanged(data?: any) {
    if (!this.userForm) {
      return;
    }
    if (data) {
      this.count_password = data.password.length;
    }
    const form = this.userForm;
    for (const field in this.formErrors) {
      if (Object.prototype.hasOwnProperty.call(this.formErrors, field)) {
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (Object.prototype.hasOwnProperty.call(control.errors, key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }

  loginUser() {
    this.authenticationService.loginWithEmailPassword(this.userForm.controls.email.value,
      this.userForm.controls.password.value).then(data => {
      this.router.navigate(['auth']);
    }).catch(error => console.log(error));
  }

}
