import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {AuthenticationGuard} from '../services/authentication.guard';

const routes: Routes = [
  { path: '', loadChildren: '../pages/login/login.module#LoginModule' },
  { path: 'register', loadChildren: '../pages/register/register.module#RegisterModule' },
  { path: 'auth', loadChildren: '../auth/auth.module#AuthModule', canActivate: [AuthenticationGuard]},
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class LazyLoadModule {
}
