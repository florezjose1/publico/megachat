import {AfterViewInit, ChangeDetectionStrategy, Component, Inject, ViewChild} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ImgCropperConfig, ImgCropperEvent, LyResizingCroppingImages } from '@alyle/ui/resizing-cropping-images';

import {LyTheme2, shadowBuilder, ThemeVariables, Platform} from '@alyle/ui';
import {Globals} from '../../globals';

export interface DialogData {
  message: string;
  imageURL: any;
}

const styles = (theme: ThemeVariables) => ({
  actions: {
    display: 'flex',
    justifyContent: 'center'
  },
  cropping: {
    maxWidth: '400px',
    height: '300px',
    margin: '0 auto'
  },
  flex: {
    flex: 1
  },
  button: {
    margin: '1em'
  },
  labelBefore: {
    paddingAfter: '8px'
  },
  item: {
    padding: '16px',
    textAlign: 'center',
    background: theme.background.secondary,
    boxShadow: shadowBuilder(1),
    borderRadius: '4px',
    height: '100%'
  },
  link: {
    color: theme.text.secondary
  }
});

@Component({
  selector: 'app-dialog-add-image-chat',
  templateUrl: './dialog-add-image-chat.component.html',
  styleUrls: ['./dialog-add-image-chat.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false
})
export class DialogAddImageChatComponent implements AfterViewInit {
  classes = this.theme.addStyleSheet(styles);

  croppedImage?: string;
  result: string;
  scale: number;
  @ViewChild(LyResizingCroppingImages) cropper: LyResizingCroppingImages;
  myConfig: ImgCropperConfig = {
    autoCrop: true,
    width: 250, // Default `250`
    height: 250, // Default `200`
    // fill: '#ff2997', // Default transparent if type = png else #000,
    type: 'image/jpeg'
  };

  appearance: 'outlined';

  message = '';

  constructor(public global: Globals, private theme: LyTheme2,
              public dialogRef: MatDialogRef<DialogAddImageChatComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    console.log('Data: ', data);
    this.data.message = this.message;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onKey(event: any) {
    this.data.message = event.target.value;
  }

  ngAfterViewInit() {
  }

  onCropped(e: ImgCropperEvent) {
    this.croppedImage = e.dataURL;
    this.data.imageURL = this.croppedImage;
  }
  onloaded(e: ImgCropperEvent) {
    this.data.imageURL = this.croppedImage;
  }
  onerror(e: ImgCropperEvent) {
    console.warn(`'${e.name}' is not a valid image`, e);
  }

}
